# Introduction

##   History

- 1961: John Mc Carthy, inventor of LISP: "The compter utility could become the basis of a new and important industry"
- 1969: Leonard Kleinrock, ARPANET project
- 2005-6: Jeff Bezos, Amazon - first to let users rent data storage and computer time over the internet as an utility

**Utility computing** $\rightarrow$ **Cloud computing**: Today, users can pay providers ony when effectively accessing the services, based on their requirements, without the need to invest on maintaining an IT infrastructure.

## Definition

Internet-centric way of computing, cloud was used by the telecommunications industry and later came to represent the Internet.
It includes both the applications delivered over Internet as well as the infrastructure that makes it possible.

> "A model for enabling ubiquitous, convenient, **on-demand** **network access** to a shared pool of configurable computing resources [...] that can be rapidly provisioned and released with minimal management effort or service provider interaction" ^[National Institut of Standard Technology]

Criteria for identifying cloud services:

- service accessible through Internet (Web browser or web APIs)
- no capital expenditure required to get started (unless migrating...)
- you pay only for the resources you use
- unlimited scalability, i.e. illusion of infinite resources available for rent

Cloud computing includes several technologies:

- Virtualization
	- Hardware: virtualize the entire software stack
	- Application: only contain instances of the application, share (fully or partially) the underlying software stack
- Web 2.0: not only static/dynamic web pages, but also web applications 
- Service-oriented computing: **services** as platform-agnostic, self-contained components that perform a specific function and can interconnect easily by e.g. REST api.
	- 2nd gen: **microservices**

Essential characteristics:

- **On-demand self-service**: relies on **orchestration technologies** (e.g. openstack, Kubernetes), provider APIs for managing the service

- **Broad network access**: ability to access service through a multitude of devices using various broadband technologies

- **Resource pooling**: 
  - resources, physical and virtual, are dynamically assigned to the customers with location independence and (usually) no control/knowledge by customer.   
  - multiple users access the same application logic simultaneously, each one isolated with its own view, data and configuration (**multi-tenancy**).

- **Rapid elasticity**: can rapidly scale the service in size. To realistically do this, need monitoring and program logic to react by dynamically autoscaling resources, load balancing instances etc.

- **Measured services**: 
  - customer is billed based on resources used, i.e. a combination of storage used, CPU time, bandwith utilized etc
  - also needed for previous point

- **Resiliency**: ability to withstand reasonable faults/problems while still retaining an acceptable level of service


## Service models 

Limit the control of the consumer and separate their responsibilities from those of the provider.
The expertise required of the consumer decvreases along with their control of the infrastructure.

- **Infrastructure** as a service (IaaS): subscriber manages everything inside the VM, from application to OS.
- **Platform** as a s. (PaaS): subscriber manages only the application and its data, the OS and below is managed by the provider
- **Software** as a s. (SaaS): subscriber only configures their instance of application, but everything is managed by the provider
	- **Function** as a s.: extreme version of the above, subscriber only controls a single function (e.g. microservice).

The three models can be combined for different target users, e.g. IT architects can use IaaS to setup PaaS for developers, who will use it to setup SaaS for end users.

## Deployment models

- **Public** cloud: for use by general public, located on premises of a cloud provider
- **Community**: used by consumers from organizations with shared concerns (e.g. government agencies, accademies etc)
- **Private** cloud: for exclusive by a single organization or business
  - on-premise
  - virtual private cloud (VPC)
    - Amazon & Google focus on VPN features
    - IBM & HP offer more phsycal isolation
- **Hybrid**: composition of distinct cloud infrastructures bound together by some technology that enables data & application portability

## NIST reference model

- Cloud **carrier**: provides physical infrastructure to providers
- Cloud **provider**: provides services that manage hardware & virtual resources
- Cloud **auditor**: conducts independent assessment of performance, security and other characteristics of cloud services
- Cloud **broker**: manages use, performance and delivery of cloud services, negotiates relationships between providers and consumers


**Scenarios**:

- provider(s) $\leftleftarrows$ broker $\leftarrow$ consumer
- carrier $\leftarrow$ provider $\leftarrow$ consumer
- provider  $\leftrightarrow$ auditor $\leftrightarrow$ consumer

Additionally:

- **Resource administrator**: can be the consumer, provider, or a third party contractor
- **Service owner**: can be consumer or provider

## Business driver: capacity planning

strategy | type | description
-|-|-
Lag | reactive | add resources when current system is at full capacity
Lead | proactive | add resources in anticipation of demand according to an estimate
Match | proactive | add resources in small increment as demand increases

To expand capacity, two alternatives are possibles for traditionally operated businesses:

1. Extend current on-site infrastructure
    - Up-front cost for expansion
    - Annual operational overhead
2. Move to the cloud
    - Migration can be very costly
    - Annual cost of cloud resources can be significantly lower than operational overhead

Other business drivers:

- ?
- ?

## Risks

- **Security** vulnerabilities
- Reduced operational **governance control**
- Limited portability between providers (***vendor lock***)
- Multi-regional **compliance & legal** issues


# Enabling technologies

## Distributed computing

A computational model which involves breaking down a computation in order to execute it concurrently on several computing elements, which may be heterogeneous and in different locations.

A special case of this technology is **distributed data storage** and retrieval, which usually includes some mechanism of **replication** for increased reliability.

A **distributed system** is:

- a collection of computers that appear to the user as a single coherent system ^[Tanenbaum]
- a system in which components located at **networked computers** communicate [...] by **passing messages** ^[Coulouris]

Components of a distributed system:

Layer|ad-hoc components|cloud computing model
-|-|-
Applications | - | SaaS
Middleware| frameworks for distributed programming | PaaS
Operating system | IPC primitives for control & data | IaaS
Hardware | networking & parallel hardware | IaaS

### Architectural styles

#### Software architecture styles

They define the different roles of components in a distributed system:

- **Call and return**
  - Top-down, **divide-and-conquer**
    - components are procedures and subprograms
    - connections are procedure calls
  - **Layered**, e.g. TCP/IP
    - Each layer only interacts with upper & lower layer
    - User only interacts with highest (more abstracted) layer
- **Independent** components, e.g. *microservices*
  - Interaction based
    - Components have their own lifecycle, but interact to perform activities
    - Each component provides a set of services, and may require services from others
  - **Event** based
    - components are loosely coupled
    - each component exposes a collection of events others can **subscribe** to
    - when a given event occurs, a **message broker** notifies all subscribed components
- Others


Possible architecture **scaling**:

- *y-axis* scaling: **functional decomposition**
- *x-axis* scaling: horizontal **duplication**
- *z-axis* scaling: **data partitioning**

**Microservices**

The microservice architecture style implements *y-axis* scaling by structuring an application as a collection of services that are:

- **loosely coupled**: independently deployable, maintainable and testable (**continuous delivery**)
- organized around **business capabilities**
- owned by small, **autonomous teams**
- allows combining **different technologies**
- better **fault isolation**

This usually results in much easier scaling and alteration as complexity increases and new components and technologies are added.
In fact, while **monolithic** applications rely on several adapters (middleware) to connect to various other services and technologies, microservices each have private data and only expose REST APIs.

There are however disadvantages compared to the traditional monolithic architecture:

- more complex initial design (no well-defined way to decompose a system into services)
- harder to radically change as it involves a distributed system
- harder to coordinate due to involvement of several teams


#### System architecture styles

Covers the phsycal organization of components and their distribution in the system infrastructure.
Two fundamental reference styles:

- **client/server**
  - many-to-one scenarios
  - usually components are presentation (frontend), application logic & data storage (backend)
    - thin clients: application logic in backend
    - fat-client: application logic in frontend (harder to update)
  - classic two tier is suited only for limited size because of central point of load/failure (server)
  - can scale through multiple tiers, i.e. hierarchical structure of servers, or horizontally (replication)
- **peer-to-peer**
  - all components have both client and server capabilities
  - consistency/reliability through consensus algorithms
  - suitable for highly decentralized systems (e.g. *blockchain*)

### Inter Process Communication *(IPC)*

IPC is used by components to coordinate the activity of the process. It is composed of three types of messages:
 
- Remote Procedure Calls (**RPC**)
  - execution of code in remote processes
  - implies a client (caller) - server (callee) architecture
  - messages are automatically created by the components
  - **stateless**
  
- instances of **serialized objects**
  - equivalent of RPC for object-oriented programming
  - processes register a set of interfaces that are accessible remotely
  - client processes can obtain a pointer to those interfaces and use them to invoke remote procedures
  - **state management** is required, as well as lifetime control of created objects
  - marshaling can be done:
    - by *value*, i.e. through copies: prone to inconsistencies
    - by *reference*: more resource demanding as references have to be tracked
  - server-based:
    - object has own life
    - creation is user controlled
    - remote invocation is occasional
  - client-based
    - objects is created with the specific purpose of executing a remotely invoked method
    - lifetime is controlled by runtime, e.g.:
      - one per invocation
      - singleton

- **partial results** of a distributed computation.
- **message passing** (used by parallel computation)

### Service Oriented Architecture *(SOA)*

A **service** encapsulates a software component so that it:

- has explicit boundaries
- is autonomous, cna be integrated in different systems and can handle failures
- exposes schema and contracts for its messages
- expresses its semantic through policies (expressions that must hold true)

Two actors:

- Service **provider**: publishes the service together with schema and contract
- Service **consumer**: develops client components that bind to the provided interface

This can also be hierarchical.


Enterprise features of SOA:

- standardized service contract
- loose coupling to minimize dependencies
- incapsulation of implementation details
- reusability
- preferrably stateless
- discoverability
- composability
  - **orchestration**: single point of control
  - **coreography**: each service is independent, coordinate through messages


#### Web services in SOA: REST

RPC over HTTP, either through SOAP or REST APIs; we will focus on REST.

It follow four design principles:

- use HTTP methods ^[GET, PUT, POST, DELETE] to implement all requests accepted by a web service.
- stateless ^[e.g. for an object that implements pagination, service returns only specified page, the client has to keep track of current and/or next page]
- expose URIs that simulate a directory structure
- transfer data through either XML or JSON

Hig-level similarities with microservices, but there are differences:

- in communication:
  - SOA uses Enterprise Service Bus, a communication middleware that is a single point of failure.
  - microservices uses lightweight open-source technologies
- in data storage:
  - SOA uses a global data model with shared databases
  - microservices have each their own DB and data model

## Datacenters

A large scale complex system, usually located in places with good bandwith, political/economical stability, and where it is easier to keep the machines cool.

Challenges:

- consolidate servers, i.e. VM migration to redistribute load for maintainance or cost saving
- guarantee Service Level Agreement (SLA)
- manage hw/sw failures
- software & security updates

### Autonomic computing

Because of the scale, traditional human-centric maintainance operations are not feasable in datacenters: humans get involved only for critical decisions.

This brought the need for **autonomic computers**: systems that can **manage themselves** through high-level **objectives** assigned by administrators.

Self properties:

- self-**configuration**: accomodate varying or unpredictable conditions
- self-**healing**: remain functioning when problems arise, e.g. replace a container in its pool as soon as it goes down
- self-**protection**: detect threats and respond accordingly (e.g. detach compromised containers, according to IDS, from network)
- self-**optimization**: constant monitoring to continually adjust to the optimal strategy

This is achieved through the following process:

1. Monitor system state
1. Analyze data against current goal
1. Plan an action to keep the system in the desired state
1. Execute selected action

Datacenters have specialized platforms to automate various tasks such as provisioning, patching, **continous integration and delivery** (CI/CD): one of the most common is **Ansible**.

#### `Ansible`

Open source tool for configuration management, deployment and orchestration. 

- **Agentless**, i.e. no software installation is required on remote machines, and no overhead when management is not running
- Configuration instructions are transferred (*pushed*) from the control server (where they are stored) to the host through existing channels, e.g. `ssh` for Unix.
- Groups of tasks are recorded in **playbooks**, written in `YAML`: they are *prescriptive* yet *responsive*, i.e. they state which action needs to be taken by each component, but also allow them to react to discovered information.
- Each playbook is compososed of *plays*, each a set of tasks over the hosts (*inventory*)
- Each task is a call to an Ansible module.


## Virtualization

Virtualization was developed to more efficiently utilize server resources: by assigning each server to a single application or costumer, data centers were registering large periods of underutilized resources (e.g. during night time). With virtualization, those resources could be assigned to some other activity.
This allows for much more flexibility, as well as power and space saving.

Components:

- Virtual image (guest)
- Virtualization layer (VMM or Hypervisor)
- Physical layer

Virtualization allows for:

- **increased security**: VMM can filter the activity of the guest, preventing certain operations as well as hiding specific resources from the guest environment through a separate, virtual file-system.
- **managed execution**: the resources of a single server may be shared among several VMs, or conversely several physical resources may be aggregated to satisfy the demands of a single VM. Additionally, the VMM can emulate specific architectures, e.g.for running old, non-compatible programs.
- **portability**: VMs can be exported and stored in a VM image file, which can later be booted from a different system

### OS level virtualization: containers

Evolution of `chroot` jail mechanism, which changes the filesystem root directory of a process and its children in order to prevent them from accessing file outside it. Since in Linux devices are treated as files, this can also regulate access to devices.

Stack:

- container manager
  - contained apps
  - shared libs
- host OS
- server

Containers are based on three concepts:

- **namespace**: each process can only interact with other processes of the same namespace. Used to determine what a container can see and access.
- **Cgroups**: how much resource the container can utilize is managed through control groups
- **unionFS**: can present as a single, shared file system the union of several physical file systems, possibly of diferent formats and different permissions. It allows merging of read-write and read-only fs, in which case the union appears read-write, but writes to branches that are actually read-only (*copyup*) are stored elsewhere as tmp files. Can have priority for specific branches in case of collisions. Utilized by container for providing shared libraries.

#### `Docker`

De-facto standard for container technology, can either run on bare metal or on VMs. Providers actually use the latter for more efficient managing, as well as increased security since containers are not yet secure enough for this kind of use (jailbreak attacks).

It's a client/server application, CLI uses REST API to interact with Docker daemon, which creates and manages Docker objects.
Docker containers are **runnable instances of images** that are stored in a repository (either local or remote), in order to allow for reuse as well as portability. 
When run, A R/W layer is mounted on top of the container for the runtime duration, but it is not persistent. This reduces portability (can not be exported) and also performance (it's slower than other storage), therefore better alternatives may be required:

- **bind mount**: mount a part of the host machine into a container's directory. This provides good performance but no portability between different hosts, and potential security dangers. Best used to share configuration files and build artifacts or when portability is not required.
- **volume**: dedicated storage area stored in the host machine, but isolated and maged directly by Docker. Better security, better portability as the daemon can replicate volumes in new hosts. Useful for sharing data between containers or for storing data on remote hosts/cloud provider.
- **tmpfs**: temporary shared memory useful to exchange data with host machine (not other containers). Only supported by Linux. Most useful for writing large amount of non-persistent data with best performance, or for security.

Images use a **read-only** layered template (`libcontainer` format) which specifies how to assemble the container. 

#### Docker services

An abstraction that allows to scale containers in a **swarm**, i.e. a set of daemons (managers) and containers (workers). Each service specifies how many replicas of each container should be available, along with load balancing and recovery procedures, resource constraints and more, through a `docker-compose.yml` file.

They can also define networking:

- **bridge**: default driver, container can access the internet through docker-managed access to the hosts's connection.
- **host**: uses host's connection directly (no isolation)
- **overlay**: connect multiple daemons together, allow services to communicate with each other or with standalone containers.
- **macvlan**: specific MAC VLAN is assigned to the container, so that it will act as physical device on the local network. Mostly used for compatibility with legacy applications.
- **none**: disable networking.

### System/hardware virtualization

In past architectures, user could instead directly access privileged (ISA) instructions.
In normal computers, all user calls go through OS:

$$\text{applications} \xrightarrow{API} \text{libraries} \xrightarrow{ABI} \text{OS} \xrightarrow{ISA} \text{hardware}$$

There are **privileged** (kernel mode) and ***non*-privileged** (user mode) instructions; in the past, privileged instructions had 3 levels of priority for I/O, CPU registers and other sensitive operations, but in the modern OS there is only ring 0 (kernel) and 3 (user).

Typical system virtualization stack:

1. VM 
   1. app
   2. libs
   3. guest OS
2. Hypervisor
3. Host OS
4. Server

#### Hyper-visor *(VMM)*

Although the name implies it's a software that runs *on top* of supervisor (kernel) mode, nowadays it runs in ring 0 since ISA instructions are not used anymore.
The function of the software is managing hardware on account of the VMs guest OS's, and as such it's also called Virtual Machine Manager.

- **Type-I**: runs on bare metal, *in place of* a traditional OS (Native VMs).
- **Type-II**: runs on top of a traditional OS (Hosted VMs).

1. **Dispatcher**: entry point of VMM, interacts with allocator and interpreter
2. **Allocator**: decides which resources to allocate to VMs
3. **Interpreter**: implements routines for carrying out the requested privileged instructions, through traps


**Formal requirements**

- **Equivalence**: guests running on a VMM should have same behaviour as when run on a physical host
- **Resource control**: VMM should have complete control of virtualized resources
- **Efficiency**: Majority of machine instructions should not require intervention from the VMM

#### Full virtualization

VMM scans the instruction stream: noncritical instructions are passed directly to the hardware, while privileged instructions are trapped into the VMM, which then emulates them in a safe(r) way. OS is unaware that it is being virtualized, however binary translation is costly, praticularly for I/O.

Emulation is done through **binary translation**, i.e. the sequence of instructions is translated from source to target architecture in one of two ways:

- **static**: attempts to fully translate the executable file without running the code first
- **dynamic**: code is translated in short sequences, and is cached and referenced whenever possible (e.g. with branch instructions pointing to already translated code). More overhead, but smart caching can make it efficient.

#### Hardware assisted virtualization

CPU manifacturers have developed technologies (Intel `VT`, AMD `V`) to minimaze the overhead of emulating x86 architecture: this is done by the addition of dedicated instructions for the VMMs. Most modern VMMs leverage this.

#### Paravirtualization

This kind of VMMs are non-trasparent to the guest OS: the manager provides specially modified APIs to replace non-virtualizable (i.e. critical) OS instructions by hypercalls. This method is most efficient since it does not need runtime translation, but requires guest OS to be modified, therefore compatibility and portability is limited and/or costly.

An example of this is provided by `Xen`, a project by Linux Foundation with support by Intel. `Xen` actually has 5 different modes combining various techniques of virtualization, the most performing being PVH (combination of full and paravirtualization).
Additionally, `KVM` is another para-virtualization tool which is part of the Linux kernel.

#### VM Migration

It may be desirable to migrate a VM from a host to another, most often for load balancing or effiency reasons.

- Off-line: VM is stopped, moved then restarted. This is fairly easy.
- Live: moved without generating service discontinuity ^[in practice, there is a stop of some milliseconds after final pages are copied, while requests are re-routed to the new host]

![VM migration ^[from the slides by prof. Casalicchio]](VMmigration.jpg)

# Mechanisms

## Cloud usage monitor

- **monitoring agent**: there is an intermediary software component that collects data (mostly network usage) in a single direction (i.e. only for incoming client requests). Drawback: could introduce trfafic delay.
- **resource agent** (**push** model): the agent actively logs into a DB information based on events notified by the resources themselves (often, just the *variations* between usage state of each resource is recorded). Drawback: overloaded resources might report with delay, or simply fail to report.
- **polling agent**: periodically *requests* usage data from resources, then store metrics or metric changes in the DB


### AWS: `CloudWatch`

Based on a push model implemented with publish/subscribe mechanism called **Simple Notification Service** (SNS) . Resources publish usage metrics, the agent subscribes to the desired resources (the ones used by a given client).

CloudWatch uses **namespaces**, which are container for **metrics**, i.e. a a time oriented set of datapoints. Each metric is identified by a up to 10 **dimensions**, name-value pairs which uniquely identify it.
Statistics for each metric are collected and aggregated with various functions (min, max, avg, sum...) and **alarms** can be set to automatically initiate actions, which could be simple notifications or triggering **autoscaling**.

CloudWatch also has a **pay-per-use monitor** which generates a usage log to be used by a billing management system for billing the customer according to predefined pricing parameters.

## Load balancing

- content-blind distribution: distribute workload pseudo-randomly (e.g. round-robin)
- asymmetric distribution: assign heavier workloads to machine with higher processing power, or more currently available capacity (e.g. weighted round-robin)
- worload prioritization: worloads are scheduled, queued and distributed according to priority levels
- content-aware distribution: requests are distributed among the most fitting machines according to the particular kinds of resources requested (CPU, GPU, network traffic etc.)

Load balancers can be **one-way** (only incoming requests pass through the load balancer, easier to implement but can be a bottleneck) or **two-way** (all requests go through the load balancer). They can also work at **layer 4** (network) or at **layer 7** (application).

### AWS: `Elastic Load Balancer`

Can perform both layer-4 and layer-7 load balancing; the latter supports more complex, app-specific rules and supports sticky sessions through cookies.
Resources are identified by instance ID or IP, and called *targets*, divided into groups each managed by a *listener*, which has a set of rules. Each group has a component that performs health checks, based on pings and configurable with many parameters (response timeout, health check interval, unhealthy/healthy treshold). Unealthy instances are detached from their pool and subsequently monitored to see if they recover.

## Autoscaling

**Scalability** measures the trend of performance with increasing load: often, it is a **nonlinear** function.
 
**Autoscaling mechanisms** perform automatic scaling of certain tasks:

- **course grain**: e.g. replicate an application
- **fine grain**: e.g. replicate a micro-service or other small application component

Tipically follows **MAPE** cycle:

1. Monitor
2. Analyze
3. Plan
4. Execute

### Autoscaler architecture

**Listener**: receives and distributes requests to available instances, request replication through a new instance of service/app when necessary (according to workload counter, or performance metrics)

 1. monitoring component
 2. scaling algorithm: decides how many additional resources can be provided

**scaling algorithms**:
	
- *treshold*-based (reactive): when a workload or resource utilization treshold is exceeded, a scaling action is triggered. Can approximate proactive by using multiple tresholds. Simple, but always an approximation of the best configuration.
- *model*-based (reactive): uses a mathematical model to compute the scaling action to perform. Could be an euristic, or could compute the real optimal action.
- *proactive*: tries to **predict** the workload or resource utilization in the short term (5-10 min) and scales accordingly, either treshold or model based. Can also incorporate a reactive element to fine-tune the configuration if the prediction was not completely correct.

### AWS: `Elastic Compute Cloud` (EC2)

Instances that share similar characteristics are put into **autoscaling groups**, logical groups for the purpose of autoscaling. 
Whenever an autoscaling action is triggered, it runs a **launch configuration**, a template for each group with specific actions to take.

The **scaling policy** can be:

- static
- manual
- **scheduled** autoscaling
- **dynamic** scaling (autoscaling based on workload/resource usage)
  - treshold based: can specify either adjustement (flat or %) or new desired capacity
    - simple: has a **cool-down** period after a scaling action, before a new scaling action is taken if necessary
    - step scaling: no cool-down period, faster reaction to changing conditions
  - model based
    - **target tracking**: the algorithm autoscales resources in order to maintain the target metric as desired

Each group should have at least one *scale-in* (decrease) and one *scale-out* (increse) policy, with a minimum and maximum group size.

Scaling is taken on **aggregated metrics** (averages), not individual VM metrics, and usually triggers only if a treshold is crossed for *at least* a certain amount of time, to prevent scaling too often due to very short spikes.

A new instance will not be immediately operative at 100% efficiency, therefore during this initial period (**warmup**) they are not computed into the aggregated metric.

![autoscaling cycle ^[from the slides by prof. Casalicchio]](autoscalingCycle.jpg){ width=70% }


### Autoscaling in containers

Docker swarms allow to maintain a certain capacity, but other components called **orchestrators** are required for selecting, deploying, monitoring and dynamically scaling the configuration of a multicontainer application.

The most used orchestrator is **Kubernetes**.

- **kube-controller**: manages the cluster of containers, decides on autoscaling (how many containers to deploy)
- **cloud-controller**: interacts with the hosting cloud service
- **kube-apiserver**: intermediary between controllers, the scheduler, and *kubelets* (nodes)
- **kube-scheduler**: decides where and when (in time) to deploy a container

The controller uses a step scaling policy called **Horizontal Pod Autoscaling**, an algorithm based on CPU utilization of the pods (avg of last minute) divided by target CPU for each pod.

## Availability zones

Cloud services can be deployed either in a single or multiple **availability zones**: each zone represent a data centre, and each **region** can have its own pricing and can contain multiple availability zones. Each zone is isolated from most failures to other zones, therefore it may be desirable to use multiple zones for reliability. For maximum reliability, however, zones in different regions should be used.

# Cloud storage

Common goals:

- scaling on demand
- high availability
- simple integration with webapps

**Atomicity**:

- requirement for an operation that should complete without any interruption
- usually requires HW support (*test-and-set*, *compare-and-swap*) and mechanism for mutual exclusion (locks, semaphores)
- two types:
  - **all-or-nothing**: there is a pre-commit phase, composed of operations that *can* be undone (e.g. lock resources, allocate memory). If it completes succesfully, enter commit point, after which *irreversible* actions are taken. If pre-commit does not complete, abort. Logs/journals are needed to ensure consistency.
  - **before-or-after**: from the point of view of the invoker of the action, each action with this kind of atomicity will behave as if it had completed either completely before or completely after any other concurrent action


## File systems

Desired properties for physical storage:

- read/write coherence: always read the last update of the value. Not trivial due to buffering techniques.
- atomicity

Types:

- **cell** storage
  - closely reflects layout of data structure in physical storage
  - each cell of equal size, hosting a single "object"
  - R or W operations work on sector or block
  - guaranteed R/W coherence
  - *before-or-after* atomicity
- **journal** storage
  - each cell has a manager (*journal*) which logs all transactions for the cell
  - processes interact with journal, not directly with cell
  - journal can be told to R,W, plus start, commit or abort an action
  - implements *all-or-nothing* atomicity: changes are first stored into the log, then passed to actual memory only when committed
  - journal allows to recover from failure

For distributed settings like cloud computing, journal storage is vital.

- **NFS** (Network File System): distributed, single point of failure (no redundancy)
- **SAN** (Storage Area Network): flexible and resilient, decouples storage and compute nodes. Widely used.
- **PFS** (Parallel File System): allow concurrent access to distributed files. Often built on top of SANs.

### NFS

When a file is required, ask the remote file system client to locate the file on the network, retrieve it and make a local copy. After commit, update the original.

- client/server model
- RPC interaction
- does not scale well

### General Parallel File System

- Allow multiple concurrent R/W operations.
- Max size of 4PB (4096 TB)
- journal model
- each I/O node has its own log
- uses RAID redundancy for fault-tolerance

### Google FS (GFS)

- a collection of 64MB *chunks*, stored as files on Linux file systems built on low-cost components
  - optimized for large files
  - less fragmentation if files grow in size often, but more wasted space for small files
  - massively reduced amount of metadata over traditional <=2MB blocks
  - increased chance that subsequent operations are within the same chunk, so fewer request to locate new blocks
- configurable **replication**, default is 3 copies
- designed and optimized for the cloud
- high reliability and fault tolerance
- prefers **append** and **sequential read** operations, to better deal with large (often streamed) data files
  - random write is almost never used
  - read is mostly sequential
  - **atomic append** is available to enable concurrent appends with minimal overhead
  - no additional data caching besides Linux's native buffer cache for each chunk
  - clients *do* cache metadata
- sustained **bandwidth** preferred over low latency, as it's mostly used to process data in bulk
- relaxed consistency model, but *transparent* to the application
- changes are not visible until recorded over the requested replicas


The main idea of the architecture is to separate file system control from data transfer, which (coupled with the large chunk size) allows for a single master node without making it a bottleneck:

1. single **master node** 
  - controls a large set of chunk servers through *HeartBeat* messages
  - has filenames, access control info, location of replicas, location and state of each server etc
  - all metadata is in memory, no disk accesses are performed for speed
  - periodically checkpoints its state (**snapshot**) to minimize recovery time in case of failure: only need to replay records after checkpoint
  - performs garbage collection and chunk migrations
  - performs all *file namespace mutations*, which are atomic
2. **chunk servers**: handle actual R/W operations onto physical storage

Details of a write operation:

1. client request master node for a *lease* to a primary chunk server
2. master replies with assigned **primary** server, plus location of replicas (**secondary** servers)
3. client send data to all interested chunk servers
4. servers buffer the data
5. send ACK to client
6. client sends write request to primary server
7. client performs required operation and sends write request to secondary servers
8. secondary servers perform operation and ACK to primary
9. primary server ACKs write request 

### Hadoop Distributed FS (HDFS)

- Designed for Apache Hadoop, a software system for **big data** applications
- large blocks (64-128MB)
- high-performance FS written in Java
- not fully POSIX compliant
- portable, but cannot directly mount on another FS
- includes **replication** (default 3), at least 1 on a different rack for reliability

Architecture is similar to GFS, but the client is the one informing the master of a succesful write here.

- **NameNode** (master):
  - manages FS namespace
  - stores metadata
  - controls access to files
  - logs operations
  - can have a reserve node for high availability
- DataNode (slave)
  - implements R/W operations

Nodes are stored in *racks*, which are organized into a **hadoop cluster** by using core switches.

Details of a write operation:

1. set up pipeline
   1. client sends W request, NameNode returns IPs of the nodes that has requested block
   2. client sends W request to primary node
   3. request is forwarded to the other replicas
2. data streaming and replication (apply changes)
3. shutdown pipe, ACK write
   1. replicas ACK to primary
   2. primary ACKs to client
   3. client informs NameNode of succesful write 


## NoSQL Databases

Traditional **relational** model databases were developed to satisfy **ACID** properties (atomicity, consistency, isolation, durability), however they usually do not scale well in speed/size.
NoSQL databases ^[also called *datastores*] are a more recent alternative specifically built with scalability in mind, though they *may not* satisfy all ACID properties - usually, they *cannot* strictly guarantee **consistency**, and instead provide some tools for reconciling consistency issues when they arise.

Three novel models:

- key-value
- document store
- graph DB

Many cloud apps are based on **Online Transaction Processing** (OLTP), which usually aim at minimizing response time through **memchaching** (caching objects in RAM). 
Scalability is also desired, and vertical scaling is always possible, in horizontal scaling however maintaining consistency is too difficult for traditional SQL databases to work. 
Instead, NoSQL works better as they can tolerate some inconsistencies through consensus-based decisions.

### Google: `BigTable`

- Tridimensional, sparse ^[columns can be empty] sorted map
- Data is indexed using row key , a column key, and timestamp
- All data is stored as *uninterpreted* **strings**
- High availability, scalability, performance
- Suited for both throughput-oriented and latency-sensitive applications
- Supports dynamic control over data layout
- Supports control over **locality** of the data, i.e. where is the physical storage of the data, to optimize data location for minimum latency 
- Uses a high-availability persistent lock service (***Chubby***) which also contains access control tables and reports dead tablet servers

**Rows**:

  -  potentially up to 64 KB, typically 10-100 byte
  - ordered lexicographically
  - R/W is atomic
  - a row range is partitioned into **tablets** for load balancing
  - short row ranges are usually on the same location

**Columns**:

- column keys are grouped into **column families**, which should contain data of the same type for better compression
- each key is a *family:qualifier* pair
- adding/removing families is costly and should be infrequent
- can implement access control

**Timestamps**:

- used to store multiple versions of the same data
- 64-bit integers, either automatic (real time) or manually supplied
- reverse-ordered for faster access to most recent
- options for garbage collection:
  - keep only last $n$ versions
  - keep versions for a given period

**SSTable**: file format use to store `BigTable` data.
It's a sequence of blocks with a block index used to locate them, which is loaded in memory when the table is opened for fast binary searching). Blocks are then accessed with a single seek on disk.
The entire table can also be loaded into memory for maximum speed if required.

The tables are organized in an architecture with a **master server** (potentially replicated) and several **tablet servers**. The master assign tablets to servers, manages load balancing and garbage collection and handles schema changes. Tablet servers manages its set of tablets, handle their R/W requests, and potentially splits tablets that have grown too large.

Google File System

### AWS: `Dynamo`

- High-availability distributed key-value store
- Used for services that require very high **reliability**
- **Weak consistency**, i.e. an operation succeds if the *majority* (not all) of the replicas are updated successfully. This also means lower latency (no need to wait for the slowest nodes) .
- Symmetrical & decentralized: all nodes have same responsibilities

Uses **optimistic replication** techniques: replicas can diverge temporarely, but eventually updates propagates and consistency is restored, at read time, either by the data store or by the application.
At least one replica is always kept available for incoming operations while others are being updated.

Operations permitted are:

- `Get(key)`: locates object replicas with requested key, returns single object or list with conflicting versions and relative context. May return an object that has not been updated yet.
- `Put(key, context, object)`: write object replicas to disk with provided key and system metadata (context), including object version. Returns immediately after, updates to replicated copies are asyncronous

If multiple update happen simultaneously, when the value needs to be read, the **reconciliation** (conflict resolution) is done by contacting the server that made the last non-diverging change and having it decide how to combine its value with the diverging changes.

Data is **partitioned** through hashing to a ring-like space of **virtual** nodes . This is so that if a physical node becomes unavailable, its load is distributed equally among the working nodes, while adding another node causes the existing nodes to share some of their load to the new node.
Each key is replicated (usually 3 copies in total) for availability and reliability,  on the following nodes of the virtual ring.


# Big Data

Term coined ~2012 to identify new applications that would deal not only deal with huge volumes of data, but the data would also be **heterogeneous** as well as **changing rapidly**, which means that **data stream processing** may be required over **batch processing**. This also causes potential problems on **veracity** of the data.

**Parallel programming**: break processing into tasks that can be concurrently executed. Not suited to all problems.

*Single program multiple data*: simplest scenario, no dependency among data, use a master-worker model

1. master process splits data in chunks
2. master assigns a worker process to each chunk (they all do the same work)
3. master collects & combines results from workers

**Divide and conquer**: can also keep splitting the problem recursively, until it can be trivially solved.
Each worker can be a core, a multicore CPU, or even multiple machines in a cluster.

Sharing results between worker has limitations:

- requires management of common state
- syncronization & deadlocks
- finite bandwidth
- temporal dependencies

## Google: `MapReduce`

`Map Reduce` (by Google) is a framework that hides implementation details for using divide and conquer, providing higher level APIs so that programmers don't have to deal with parallelization, load balancing or fault tolerance.
MR implements an alternative, **share-nothing** approach, which is much easier to scale due to much smaller overhead.

1. **Map**: 
  - executes some function on a set of key-value pairs (*input shard*), produce a new set of pairs
  - map functions are distributed across compute nodes in a transparent way to the programmer, by partitioning input into shards
2. Optionally, there can be a **combine** functions before shuffle, sometimes similar to the reduce function.
3. **Shuffle and sort**: an implicit, distributed *group by* operation, groups intermediary values associated with same key
4. **Reduce**: 
  - combines values in sets to create a new value
  - executed in parallel over each output key

Each MapReduce *job* (program) includes code for map & reduce, configuration params and input dataset on a distributed filesystem.
Each job is divided into tasks (mappers and reducers), reducers can only be completed after mappers. There may be multiple map&reduce steps.

Fault tolerance:

- if a *master* node fails, computation is lost and must be restarted
- if a *map* worker fails, all its assigned computations must be redone
- if a *reduce* worker fails, simply reschedule the reduce task on another worker

## Apache `Hadoop`

- A open-source ^[originaly by Yahoo] framework for distributed processing of data across clusters of computers
- Highly scalable
- Can detect and **handle failure** at **application level**, instead of requiring hardware failure detection

Core components:

- HDFS
- Hadoop **Yarn**, a cluster managemnt system
- Hadoop MapReduce, an implementation of MapReduce

Multitude of related projects:

- Apache Pig: data analysis
- Apache Hive: software for large distributed SQL DBs
- Apache Hibase: distributed, versioned no-SQL DB inspired by BigTable

Hadoop runs on clusters, typicaly several racks of 8-64 nodes (can be PCs):

- Uses commodity hardware, cheap to deploy
- **Inter-rack bandwidth** may be a bottleneck.
- significant failure rate, therefore use redundancy

### YARN *(Yet Another Resource Negotiator)*

It is the real core of Hadoop: a resource manament system to deploy and execute MapReduce tasks on nodes across racks.
Uses several daemons to manage applications (either single job, or DAG of jobs):

- global **ResourceManager** (RM): arbitrates resources among all applications in the system
  - scheduler: assigns resources based on requirements, does not track and if necessary restart jobs that fail. Scheduling policies are provided by plugins.
    - capacity scheduler: maximize throughput
    - fair scheduler: equal share of resources (on average) among applications
  - application manager: accepts incoming jobs and negotiates (with NMs) the allocation of a AM, which will start the first container. Also restarts failed jobs.
- per-machine **NodeManager** (NM): responsible for resource containers, monitors usage and reports to RM
- per-application **ApplicationMaster** (AM): framework-specific library, requests resource allocation to RM, works with NM to execute and monitor tasks

There is a **ReservationSystem**, used to specify a profile of resources needed to ensure that certain jobs can finish within given temporal constraints (meaning they may require priority, depending on the constraint).

ResourceManagers can be **federated** to scale the system by combining multiple independent clusters.

### Hadoop's `MapReduce`

**Locality** optimization: Map tasks are usually I/O bounded (have to read data from disk), therefore best performance is usually when map task is run on same node that stores the needed data. If not possible, use *rack-local* execution (which saves the use of intra-rack bandwidth), and finally *off-rack* if that is not possible either.

There is a **partitioner** which determines how to partition the output of the map operations when there are multiple reducers; all records for an intermediate key are put into the same partition.

The **combine** task is usually a locally-run short reduce task meant to minimize data transfer between nodes performing map and reduce operations, usally by performing data aggregation.

By default, Hadoop uses Java programs structured as follows:

- `main` method: set # of reducers, choose mapper, partitioner and reducer classes, configure other Hadoop parameters
- Mapper class: $(k,v)$ pairs $\rightarrow (k,v)$ pairs
- Reducer class: $(k,$ `iterator`$[v])$ pairs $\rightarrow (k,v)$ pairs

But other languages can be used through Hadoop streaming API, which use UNIX `stdin/stdout` to communicate between the components.

## Apache `Spark`

Born to resolve important limitations of MapReduce:

- not *everything* can be solved by the map-reduce approach
- sometimes simple operations would require an excessive numbers of map & reduce steps
- simple, but very rigid 
- no native support for iterations (each iteration reads/writes from/to disk, high overhead)

Based on DAG ^[Direct Acyclic Graph], Spark is now the leading platform for large-scale SQL, batch, stream and ML processing. It works mainly using **in-memory processing** of data (up to 10x faster than Hadoop), meaning data is loaded from disk only once.

Spark stack:

- **Spark Core**: basic functionalities and data abstraction
  - fault recovery, memory management, interaction with storage systems
  - Resilient Distributed Dataset (RDD): collection of distributed item which allow parallel work over many nodes
  - written in Scala, API for Java, Python, R
- Standalone scheduler
- Spark SQL: working with structured data
- Spark Streaming: real time processing
- MLlib: functions for ML such as feature extraction, classification, regression, clustering
- GraphX: functions for manipulation of graphs

Cluster management can be:

- standalone mode: simple FIFO scheduler
- external resource manager
  - YARN
  - Apache Mesos
  - Kubernetes


A Spark application is a set of independent processes that run on a cluster, coordinated by a shared **SparkContext** (in the Driver Program), which specifies how to create the original object (in RDD form) and how to do computations on it.

**RDD**:

- distributed and fault tolerant
  - automatically rebuild RDD on failure
  - no actual replication of the RDD
- immutable, can only be modified by creating a new RDD
- cached in memory across cluster (each node contains at least a portion of it)
- can be manipulated in parallel, each node works on its portion
  - only use **parallelizable operators**, i.e. **higher order functions** that execute user-defined functions in parallel
  - a job is described as a DAG of such operators
  - can automatically choose number of partitions, or manually specify
  - typically Spark assigns 2 partitions to each CPU in the cluster
- can be created in 3 ways
  - `parallelize`: load existing collections on host
  - `textFile`: load a file on the file system, e.g. HDFS
  - `map`/`filter`/`flatMap` ^[the flat map is a special map which can produce 0 or more output values for each input]: apply some transformation operation to an existing RDD to obtain a new RDD
- can be manipulated in 2 ways:
  - **transformations**: create a new RDD in a *lazy* way, i.e. only executed on-demand when *actions* are taken on the RDD, as they which require an output. This allows for more efficient grouping of several operations in a single pass, if possible.
  - **actions** (`take`,`count`,`collect`,`save`,`reduce`...): transforms an RDD into a scalar or vector
- to reduce number of times an RDD is re-computed on-demand, can choose to store a modified RDD on disk in addition to (or instead of) memory, *serialized* (space efficient) or *unserialized*, through `persist()` or `cache()`

Spark uses a master-slave architecture:

- **Driver Program**: the master, runs user's main function, interacts with cluster manager to ask allocation of resources (worker nodes)
- **Cluster Manager**: allocates worker nodes
- **Worker node**: executes a task requested by driver program, store data for the application

The application DAG is compiled into **stages**, sequences of RDDS without shuffles in between: they can be executed as a series of tasks on a single partition.

**Fault tolerance**: RDDS track the series of transformations used to build them (**lineage**), which can be used to recompute it in case it gets corrupted or lost.

The **job scheduling** prioritizes data locality: when a user runs an action on an RDD, scheduler builds a DAG of stages from the lineage graph and assigns jobs based on the partitions of the RDD which are currently available in memory of each given node.

Advatages:

- applications have isolated scheduling and execution
- Spark is agnostic of how the cluster resource manager works, multiple compatible implementations possible

Disadvantages:

- no sharing of data between separate applications
- all driver programs need to be remain running and connected in order to receive results from workers at any time
- since driver program shares data with worker nodes often, it should be located as close as possible (locality) for good performance

### Spark streaming

- extension of core Spark API
- enables scalable, fault-tolerant stream processing of live data
- can push processed data to filesystems, DBs and live dashboard

Input data is actually divided into **batches**, each one used to create an RDD for use with normal Spark API.
The stream is abstracted into a **Dstream** (*discretized stream*), which is represented by Spark as a sequence of RDDs.
