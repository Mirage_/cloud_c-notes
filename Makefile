dest=cloudComputing.pdf
src=src.md

$(dest): $(src)
	pandoc $(src) -o $(dest) -s --table-of-contents -V documentclass=report --number-sections
clean:
	rm -f $(dest)